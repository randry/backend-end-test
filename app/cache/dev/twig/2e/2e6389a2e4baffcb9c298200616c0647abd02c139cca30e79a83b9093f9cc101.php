<?php

/* UploadBundle:Default:layout.html.twig */
class __TwigTemplate_35fe48b1e18c7b2a247b63a6f608049127d48957d8697ce04f8940d545b7efc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'jsFile' => array($this, 'block_jsFile'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5a91a3f3cb3d2efdb20473e3de170de77ec3429845596e2b87d55bc862a14ef = $this->env->getExtension("native_profiler");
        $__internal_a5a91a3f3cb3d2efdb20473e3de170de77ec3429845596e2b87d55bc862a14ef->enter($__internal_a5a91a3f3cb3d2efdb20473e3de170de77ec3429845596e2b87d55bc862a14ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UploadBundle:Default:layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css\">-->
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/styles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "        ";
        $this->displayBlock('jsFile', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <div id=\"container\">
            <div id=\"boxHeader\">";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["titleHeader"]) ? $context["titleHeader"] : $this->getContext($context, "titleHeader")), "html", null, true);
        echo "</div>
            <div id=\"boxNavigation\">
                    <ul id=\"boxLi\">
                        <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getUrl("upload_profilListPage");
        echo "\"><i class=\"fa fa-table\"></i>LIST</a></li>
                        <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getUrl("upload_formPage");
        echo "\"><i class=\"fa fa-upload\"></i>UPLOAD</a></li>
                        <div class=\"clearBox\"></div>
                    </ul>
            </div>
            <div id=\"boxBody\">
                <div id=\"boxContain\">
                    <div id=\"boxFlashMessage\">
                        ";
        // line 26
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array(), "any", false, true), "flashbag", array(), "any", false, true), "get", array(0 => "errorList"), "method", true, true)) {
            // line 27
            echo "                            ";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "errorList"), "method")) > 0)) {
                // line 28
                echo "                                <ul>
                                    ";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "errorList"), "method"));
                foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
                    // line 30
                    echo "                                        <li>";
                    echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
                    echo "</li>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 32
                echo "                                </ul>   
                            ";
            }
            // line 34
            echo "                        ";
        }
        // line 35
        echo "                    </div>
                    <div id=\"contain\">
                        ";
        // line 37
        $this->displayBlock('body', $context, $blocks);
        // line 38
        echo "                    </div>
                </div>
            </div>
        </div>
            ";
        // line 42
        $this->displayBlock('javascripts', $context, $blocks);
        // line 43
        echo "    </body>
</html>";
        
        $__internal_a5a91a3f3cb3d2efdb20473e3de170de77ec3429845596e2b87d55bc862a14ef->leave($__internal_a5a91a3f3cb3d2efdb20473e3de170de77ec3429845596e2b87d55bc862a14ef_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_e22fa29f24aae9bd0ed48c037d16e5e4db52c1116c59a27dd6a649c2d2022071 = $this->env->getExtension("native_profiler");
        $__internal_e22fa29f24aae9bd0ed48c037d16e5e4db52c1116c59a27dd6a649c2d2022071->enter($__internal_e22fa29f24aae9bd0ed48c037d16e5e4db52c1116c59a27dd6a649c2d2022071_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, (isset($context["titlePage"]) ? $context["titlePage"] : $this->getContext($context, "titlePage")), "html", null, true);
        
        $__internal_e22fa29f24aae9bd0ed48c037d16e5e4db52c1116c59a27dd6a649c2d2022071->leave($__internal_e22fa29f24aae9bd0ed48c037d16e5e4db52c1116c59a27dd6a649c2d2022071_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7b387fc3a4f5dd57823c1345346d7e9200919fb5b954545c98ae8266fee8613c = $this->env->getExtension("native_profiler");
        $__internal_7b387fc3a4f5dd57823c1345346d7e9200919fb5b954545c98ae8266fee8613c->enter($__internal_7b387fc3a4f5dd57823c1345346d7e9200919fb5b954545c98ae8266fee8613c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_7b387fc3a4f5dd57823c1345346d7e9200919fb5b954545c98ae8266fee8613c->leave($__internal_7b387fc3a4f5dd57823c1345346d7e9200919fb5b954545c98ae8266fee8613c_prof);

    }

    // line 10
    public function block_jsFile($context, array $blocks = array())
    {
        $__internal_04581cb63d94761844a0b3a1d92b56419071542339928f9eb700a136f2b1f5f3 = $this->env->getExtension("native_profiler");
        $__internal_04581cb63d94761844a0b3a1d92b56419071542339928f9eb700a136f2b1f5f3->enter($__internal_04581cb63d94761844a0b3a1d92b56419071542339928f9eb700a136f2b1f5f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jsFile"));

        
        $__internal_04581cb63d94761844a0b3a1d92b56419071542339928f9eb700a136f2b1f5f3->leave($__internal_04581cb63d94761844a0b3a1d92b56419071542339928f9eb700a136f2b1f5f3_prof);

    }

    // line 37
    public function block_body($context, array $blocks = array())
    {
        $__internal_c50b032d9fc3f9af396cafd954ef3d49064d90ccc0953a37972270b3910d4ff0 = $this->env->getExtension("native_profiler");
        $__internal_c50b032d9fc3f9af396cafd954ef3d49064d90ccc0953a37972270b3910d4ff0->enter($__internal_c50b032d9fc3f9af396cafd954ef3d49064d90ccc0953a37972270b3910d4ff0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_c50b032d9fc3f9af396cafd954ef3d49064d90ccc0953a37972270b3910d4ff0->leave($__internal_c50b032d9fc3f9af396cafd954ef3d49064d90ccc0953a37972270b3910d4ff0_prof);

    }

    // line 42
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f46b58d3ef38891bca60e0bfc865cb4d9e8778e6107459d3a4fdec080fafb9c4 = $this->env->getExtension("native_profiler");
        $__internal_f46b58d3ef38891bca60e0bfc865cb4d9e8778e6107459d3a4fdec080fafb9c4->enter($__internal_f46b58d3ef38891bca60e0bfc865cb4d9e8778e6107459d3a4fdec080fafb9c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_f46b58d3ef38891bca60e0bfc865cb4d9e8778e6107459d3a4fdec080fafb9c4->leave($__internal_f46b58d3ef38891bca60e0bfc865cb4d9e8778e6107459d3a4fdec080fafb9c4_prof);

    }

    public function getTemplateName()
    {
        return "UploadBundle:Default:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 42,  163 => 37,  152 => 10,  141 => 9,  129 => 5,  121 => 43,  119 => 42,  113 => 38,  111 => 37,  107 => 35,  104 => 34,  100 => 32,  91 => 30,  87 => 29,  84 => 28,  81 => 27,  79 => 26,  69 => 19,  65 => 18,  59 => 15,  51 => 11,  48 => 10,  46 => 9,  42 => 8,  38 => 7,  33 => 5,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}{{ titlePage }}{% endblock %}</title>*/
/*         <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->*/
/*         <link href="{{ asset('bundles/framework/font-awesome-4.5.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />*/
/*         <link href="{{ asset('bundles/framework/css/styles.css') }}" rel="stylesheet" type="text/css" />*/
/*         {% block stylesheets %}{% endblock %}*/
/*         {% block jsFile %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         <div id="container">*/
/*             <div id="boxHeader">{{ titleHeader }}</div>*/
/*             <div id="boxNavigation">*/
/*                     <ul id="boxLi">*/
/*                         <li><a href="{{ url('upload_profilListPage') }}"><i class="fa fa-table"></i>LIST</a></li>*/
/*                         <li><a href="{{ url('upload_formPage') }}"><i class="fa fa-upload"></i>UPLOAD</a></li>*/
/*                         <div class="clearBox"></div>*/
/*                     </ul>*/
/*             </div>*/
/*             <div id="boxBody">*/
/*                 <div id="boxContain">*/
/*                     <div id="boxFlashMessage">*/
/*                         {% if app.session.flashbag.get('errorList') is defined %}*/
/*                             {% if app.session.flashbag.get('errorList')|length > 0 %}*/
/*                                 <ul>*/
/*                                     {% for flashMessage in app.session.flashbag.get('errorList') %}*/
/*                                         <li>{{ flashMessage }}</li>*/
/*                                     {% endfor %}*/
/*                                 </ul>   */
/*                             {% endif %}*/
/*                         {% endif %}*/
/*                     </div>*/
/*                     <div id="contain">*/
/*                         {% block body %}{% endblock %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*             {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
