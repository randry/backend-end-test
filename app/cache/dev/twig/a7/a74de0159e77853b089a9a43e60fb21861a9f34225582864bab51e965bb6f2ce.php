<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_f36a01c336f50406d0477ff0cb1359cc1b8ab74025752fa2fdeb058830d30c4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa52e43cae242f8042e5987932451a1f94b55dfdf515e4fee2ccd38c801229a7 = $this->env->getExtension("native_profiler");
        $__internal_fa52e43cae242f8042e5987932451a1f94b55dfdf515e4fee2ccd38c801229a7->enter($__internal_fa52e43cae242f8042e5987932451a1f94b55dfdf515e4fee2ccd38c801229a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa52e43cae242f8042e5987932451a1f94b55dfdf515e4fee2ccd38c801229a7->leave($__internal_fa52e43cae242f8042e5987932451a1f94b55dfdf515e4fee2ccd38c801229a7_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_f3737f373f1d007f4572674069dac1431cf53b5cc7ce5ff1ddbb22659bbfd021 = $this->env->getExtension("native_profiler");
        $__internal_f3737f373f1d007f4572674069dac1431cf53b5cc7ce5ff1ddbb22659bbfd021->enter($__internal_f3737f373f1d007f4572674069dac1431cf53b5cc7ce5ff1ddbb22659bbfd021_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f3737f373f1d007f4572674069dac1431cf53b5cc7ce5ff1ddbb22659bbfd021->leave($__internal_f3737f373f1d007f4572674069dac1431cf53b5cc7ce5ff1ddbb22659bbfd021_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7d451649d186e1b921b63946b0c372fcf68e61dbdfd5fc9d1879711c8dbc7ea9 = $this->env->getExtension("native_profiler");
        $__internal_7d451649d186e1b921b63946b0c372fcf68e61dbdfd5fc9d1879711c8dbc7ea9->enter($__internal_7d451649d186e1b921b63946b0c372fcf68e61dbdfd5fc9d1879711c8dbc7ea9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_7d451649d186e1b921b63946b0c372fcf68e61dbdfd5fc9d1879711c8dbc7ea9->leave($__internal_7d451649d186e1b921b63946b0c372fcf68e61dbdfd5fc9d1879711c8dbc7ea9_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_aa26f2d113ab1ad4ff01f7cb76a30a93bbf0487ca1ac13f88e6e29e065f1e703 = $this->env->getExtension("native_profiler");
        $__internal_aa26f2d113ab1ad4ff01f7cb76a30a93bbf0487ca1ac13f88e6e29e065f1e703->enter($__internal_aa26f2d113ab1ad4ff01f7cb76a30a93bbf0487ca1ac13f88e6e29e065f1e703_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_aa26f2d113ab1ad4ff01f7cb76a30a93bbf0487ca1ac13f88e6e29e065f1e703->leave($__internal_aa26f2d113ab1ad4ff01f7cb76a30a93bbf0487ca1ac13f88e6e29e065f1e703_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
