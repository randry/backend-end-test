<?php

/* UploadBundle:Form:upload.html.twig */
class __TwigTemplate_583722ed4a959c2f12826106cda433d0842e15eb49b742215c1da07e9037b8eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("UploadBundle:Default:layout.html.twig", "UploadBundle:Form:upload.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UploadBundle:Default:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30028d3209f848810b01eba0f9e8c0fb7f50d25aaab0a9b53664a8c3822ca2ec = $this->env->getExtension("native_profiler");
        $__internal_30028d3209f848810b01eba0f9e8c0fb7f50d25aaab0a9b53664a8c3822ca2ec->enter($__internal_30028d3209f848810b01eba0f9e8c0fb7f50d25aaab0a9b53664a8c3822ca2ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UploadBundle:Form:upload.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_30028d3209f848810b01eba0f9e8c0fb7f50d25aaab0a9b53664a8c3822ca2ec->leave($__internal_30028d3209f848810b01eba0f9e8c0fb7f50d25aaab0a9b53664a8c3822ca2ec_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_84524ac9c70dd99156cfe217c862f1b72c67f9230c0c889324630f51c0178e40 = $this->env->getExtension("native_profiler");
        $__internal_84524ac9c70dd99156cfe217c862f1b72c67f9230c0c889324630f51c0178e40->enter($__internal_84524ac9c70dd99156cfe217c862f1b72c67f9230c0c889324630f51c0178e40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t<div id=\"boxUpload\">
\t\t";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
\t</div>
\t<div id=\"boxError\">
\t\t";
        // line 8
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 9
            echo "\t\t    <ul>
\t\t        ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")));
            foreach ($context['_seq'] as $context["_key"] => $context["errorMessage"]) {
                // line 11
                echo "\t\t            <li>";
                echo twig_escape_filter($this->env, $context["errorMessage"], "html", null, true);
                echo "</li>
\t\t        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['errorMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "\t\t    </ul>
\t\t";
        }
        // line 15
        echo "\t</div>
";
        
        $__internal_84524ac9c70dd99156cfe217c862f1b72c67f9230c0c889324630f51c0178e40->leave($__internal_84524ac9c70dd99156cfe217c862f1b72c67f9230c0c889324630f51c0178e40_prof);

    }

    // line 18
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3275641e117afed96a757be7c052685fbd172d818c8b142336305f266a4fe9f8 = $this->env->getExtension("native_profiler");
        $__internal_3275641e117afed96a757be7c052685fbd172d818c8b142336305f266a4fe9f8->enter($__internal_3275641e117afed96a757be7c052685fbd172d818c8b142336305f266a4fe9f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_3275641e117afed96a757be7c052685fbd172d818c8b142336305f266a4fe9f8->leave($__internal_3275641e117afed96a757be7c052685fbd172d818c8b142336305f266a4fe9f8_prof);

    }

    public function getTemplateName()
    {
        return "UploadBundle:Form:upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  72 => 15,  68 => 13,  59 => 11,  55 => 10,  52 => 9,  50 => 8,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'UploadBundle:Default:layout.html.twig' %}*/
/* */
/* {% block body %}*/
/* 	<div id="boxUpload">*/
/* 		{{ form(form) }}*/
/* 	</div>*/
/* 	<div id="boxError">*/
/* 		{% if error %}*/
/* 		    <ul>*/
/* 		        {% for errorMessage in error %}*/
/* 		            <li>{{ errorMessage }}</li>*/
/* 		        {% endfor %}*/
/* 		    </ul>*/
/* 		{% endif %}*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}{% endblock %}*/
/* */
/* */
