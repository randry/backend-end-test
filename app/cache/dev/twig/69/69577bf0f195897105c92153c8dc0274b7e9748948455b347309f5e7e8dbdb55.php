<?php

/* UploadBundle:Profil:displayList.html.twig */
class __TwigTemplate_a4b75b6a63f619c23e8cdaaf37515807e9e8535eda07d66232fd94887080ebab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("UploadBundle:Default:layout.html.twig", "UploadBundle:Profil:displayList.html.twig", 1);
        $this->blocks = array(
            'jsFile' => array($this, 'block_jsFile'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UploadBundle:Default:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_427835fc9c6ece466be235ce38f7f2121987e16ef367a4b9b34fd26aeb952561 = $this->env->getExtension("native_profiler");
        $__internal_427835fc9c6ece466be235ce38f7f2121987e16ef367a4b9b34fd26aeb952561->enter($__internal_427835fc9c6ece466be235ce38f7f2121987e16ef367a4b9b34fd26aeb952561_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UploadBundle:Profil:displayList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_427835fc9c6ece466be235ce38f7f2121987e16ef367a4b9b34fd26aeb952561->leave($__internal_427835fc9c6ece466be235ce38f7f2121987e16ef367a4b9b34fd26aeb952561_prof);

    }

    // line 3
    public function block_jsFile($context, array $blocks = array())
    {
        $__internal_6dc10c9747c236c7e9980a8eabf17132333906430ccd5f2afd44047b2128a930 = $this->env->getExtension("native_profiler");
        $__internal_6dc10c9747c236c7e9980a8eabf17132333906430ccd5f2afd44047b2128a930->enter($__internal_6dc10c9747c236c7e9980a8eabf17132333906430ccd5f2afd44047b2128a930_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jsFile"));

        // line 4
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/jquery-2.2.0.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/jquery.serializeToJSON.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/formValidation.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/formProfil.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/eventProfil.js"), "html", null, true);
        echo "\"></script>
\t
";
        
        $__internal_6dc10c9747c236c7e9980a8eabf17132333906430ccd5f2afd44047b2128a930->leave($__internal_6dc10c9747c236c7e9980a8eabf17132333906430ccd5f2afd44047b2128a930_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_9fae452c06b7e17fcd83e7fc8a87dea9823b5288ed7e4fbf1f30d7e6417247cb = $this->env->getExtension("native_profiler");
        $__internal_9fae452c06b7e17fcd83e7fc8a87dea9823b5288ed7e4fbf1f30d7e6417247cb->enter($__internal_9fae452c06b7e17fcd83e7fc8a87dea9823b5288ed7e4fbf1f30d7e6417247cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "<div id=\"boxProfil\">
\t<div id=\"boxBtnAdd\"><button id=\"btnAdd\" class=\"btn\"><i class=\"fa fa-plus\"></i>NEW PROFIL</button></div>
\t<input type=\"hidden\" id=\"baseUrl\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["baseUrl"]) ? $context["baseUrl"] : $this->getContext($context, "baseUrl")), "html", null, true);
        echo "\"/>
\t<input type=\"hidden\" id=\"actionForm\" value=\"\"/>
\t<input type=\"hidden\" id=\"idProfil\" value=\"\"/>
\t<div id=\"profilContain\">
\t\t<div id=\"boxTable\">
\t\t\t";
        // line 20
        if (array_key_exists("profils", $context)) {
            // line 21
            echo "\t        \t";
            if ((twig_length_filter($this->env, (isset($context["profils"]) ? $context["profils"] : $this->getContext($context, "profils"))) > 0)) {
                // line 22
                echo "\t\t\t\t\t<table id=\"tableProfil\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<th>FIRSTNAME</th>
\t\t\t\t\t\t\t<th>LASTNAME</th>
\t\t\t\t\t\t\t<th>GENDER</th>
\t\t\t\t\t\t\t<th>MAIL</th>
\t\t\t\t\t\t\t<th>ACTIONS</th>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t";
                // line 31
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["profils"]) ? $context["profils"] : $this->getContext($context, "profils")));
                foreach ($context['_seq'] as $context["_key"] => $context["profil"]) {
                    // line 32
                    echo "\t                            <tr>
\t                            \t<td id=\"firstname_";
                    // line 33
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "firstname", array()), "html", null, true);
                    echo "</td>
\t                            \t<td id=\"lastname_";
                    // line 34
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "lastname", array()), "html", null, true);
                    echo "</td>
\t                            \t<td id=\"gender_";
                    // line 35
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "gender", array()), "html", null, true);
                    echo "</td>
\t                            \t<td id=\"mail_";
                    // line 36
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "mail", array()), "html", null, true);
                    echo "</td>
\t                            \t<td>
\t                            \t\t<button class=\"btn btnAction btnEdit\" id=\"btnEdit_";
                    // line 38
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\"><i class=\"fa fa-pencil\"></i>EDIT</button>
\t                            \t\t<button class=\"btn btnAction btnDelete\" id=\"btnDelete_";
                    // line 39
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\"><i class=\"fa fa-trash\"></i>DELETE</button>
\t                            \t</td>
\t                            </tr>
\t                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['profil'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 43
                echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t";
            } else {
                // line 46
                echo "\t\t\t\t\t<div id=\"emptyTxt\"><i class=\"fa fa-exclamation-triangle\"></i>EMPTY LIST</div>
\t\t\t\t";
            }
            // line 47
            echo "\t\t
\t      \t";
        }
        // line 49
        echo "\t\t</div>
\t\t<div id=\"boxForm\">
\t\t\t<div id=\"boxFormProfil\"></div>
\t\t</div>
\t\t<div class=\"clearBox\"></div>
\t</div>
\t<div class=\"clearBox\"></div>
</div>
";
        
        $__internal_9fae452c06b7e17fcd83e7fc8a87dea9823b5288ed7e4fbf1f30d7e6417247cb->leave($__internal_9fae452c06b7e17fcd83e7fc8a87dea9823b5288ed7e4fbf1f30d7e6417247cb_prof);

    }

    // line 59
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5f7db391e441f03ca396bea75118a9af6640e799cef69bdab2a6ceedee0d82a6 = $this->env->getExtension("native_profiler");
        $__internal_5f7db391e441f03ca396bea75118a9af6640e799cef69bdab2a6ceedee0d82a6->enter($__internal_5f7db391e441f03ca396bea75118a9af6640e799cef69bdab2a6ceedee0d82a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 60
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/eventProfil.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t\t\$(document).ready(function(){

\t\t\teventProfil.init();

\t\t});\t
\t</script>
";
        
        $__internal_5f7db391e441f03ca396bea75118a9af6640e799cef69bdab2a6ceedee0d82a6->leave($__internal_5f7db391e441f03ca396bea75118a9af6640e799cef69bdab2a6ceedee0d82a6_prof);

    }

    public function getTemplateName()
    {
        return "UploadBundle:Profil:displayList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 60,  177 => 59,  162 => 49,  158 => 47,  154 => 46,  149 => 43,  139 => 39,  135 => 38,  128 => 36,  122 => 35,  116 => 34,  110 => 33,  107 => 32,  103 => 31,  92 => 22,  89 => 21,  87 => 20,  79 => 15,  75 => 13,  69 => 12,  59 => 8,  55 => 7,  51 => 6,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'UploadBundle:Default:layout.html.twig' %}*/
/* */
/* {% block jsFile %}*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/jquery-2.2.0.min.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/jquery.serializeToJSON.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/formValidation.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/formProfil.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/eventProfil.js') }}"></script>*/
/* 	*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/* <div id="boxProfil">*/
/* 	<div id="boxBtnAdd"><button id="btnAdd" class="btn"><i class="fa fa-plus"></i>NEW PROFIL</button></div>*/
/* 	<input type="hidden" id="baseUrl" value="{{ baseUrl }}"/>*/
/* 	<input type="hidden" id="actionForm" value=""/>*/
/* 	<input type="hidden" id="idProfil" value=""/>*/
/* 	<div id="profilContain">*/
/* 		<div id="boxTable">*/
/* 			{% if profils is defined %}*/
/* 	        	{% if profils|length > 0 %}*/
/* 					<table id="tableProfil">*/
/* 						<thead>*/
/* 							<th>FIRSTNAME</th>*/
/* 							<th>LASTNAME</th>*/
/* 							<th>GENDER</th>*/
/* 							<th>MAIL</th>*/
/* 							<th>ACTIONS</th>*/
/* 						</thead>*/
/* 						<tbody>*/
/* 							{% for profil in profils %}*/
/* 	                            <tr>*/
/* 	                            	<td id="firstname_{{profil.id}}">{{profil.firstname}}</td>*/
/* 	                            	<td id="lastname_{{profil.id}}">{{profil.lastname}}</td>*/
/* 	                            	<td id="gender_{{profil.id}}">{{profil.gender}}</td>*/
/* 	                            	<td id="mail_{{profil.id}}">{{profil.mail}}</td>*/
/* 	                            	<td>*/
/* 	                            		<button class="btn btnAction btnEdit" id="btnEdit_{{profil.id}}"><i class="fa fa-pencil"></i>EDIT</button>*/
/* 	                            		<button class="btn btnAction btnDelete" id="btnDelete_{{profil.id}}"><i class="fa fa-trash"></i>DELETE</button>*/
/* 	                            	</td>*/
/* 	                            </tr>*/
/* 	                        {% endfor %}*/
/* 						</tbody>*/
/* 					</table>*/
/* 				{% else %}*/
/* 					<div id="emptyTxt"><i class="fa fa-exclamation-triangle"></i>EMPTY LIST</div>*/
/* 				{% endif %}		*/
/* 	      	{% endif %}*/
/* 		</div>*/
/* 		<div id="boxForm">*/
/* 			<div id="boxFormProfil"></div>*/
/* 		</div>*/
/* 		<div class="clearBox"></div>*/
/* 	</div>*/
/* 	<div class="clearBox"></div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/eventProfil.js') }}"></script>*/
/* 	<script type="text/javascript">*/
/* 		$(document).ready(function(){*/
/* */
/* 			eventProfil.init();*/
/* */
/* 		});	*/
/* 	</script>*/
/* {% endblock %}*/
