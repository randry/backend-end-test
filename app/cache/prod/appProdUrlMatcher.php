<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // upload_displayListPage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'upload_displayListPage');
            }

            return array (  '_controller' => 'UploadBundle\\Controller\\ProfilController::displayListAction',  '_route' => 'upload_displayListPage',);
        }

        // upload_profilListPage
        if ($pathinfo === '/list') {
            return array (  '_controller' => 'UploadBundle\\Controller\\ProfilController::displayListAction',  '_route' => 'upload_profilListPage',);
        }

        // upload_deleteAction
        if ($pathinfo === '/delete') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_upload_deleteAction;
            }

            return array (  '_controller' => 'UploadBundle\\Controller\\ProfilController::deleteAction',  '_route' => 'upload_deleteAction',);
        }
        not_upload_deleteAction:

        // upload_addAction
        if ($pathinfo === '/add') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_upload_addAction;
            }

            return array (  '_controller' => 'UploadBundle\\Controller\\ProfilController::addAction',  '_route' => 'upload_addAction',);
        }
        not_upload_addAction:

        // upload_editAction
        if ($pathinfo === '/edit') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_upload_editAction;
            }

            return array (  '_controller' => 'UploadBundle\\Controller\\ProfilController::editAction',  '_route' => 'upload_editAction',);
        }
        not_upload_editAction:

        if (0 === strpos($pathinfo, '/get')) {
            // upload_getAllProfilAction
            if ($pathinfo === '/getAllProfil') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_upload_getAllProfilAction;
                }

                return array (  '_controller' => 'UploadBundle\\Controller\\ProfilController::getAllProfilAction',  '_route' => 'upload_getAllProfilAction',);
            }
            not_upload_getAllProfilAction:

            // upload_getProfilAction
            if ($pathinfo === '/getProfil') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_upload_getProfilAction;
                }

                return array (  '_controller' => 'UploadBundle\\Controller\\ProfilController::getProfilAction',  '_route' => 'upload_getProfilAction',);
            }
            not_upload_getProfilAction:

        }

        // upload_formPage
        if ($pathinfo === '/upload') {
            return array (  '_controller' => 'UploadBundle\\Controller\\FormController::uploadAction',  '_route' => 'upload_formPage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
