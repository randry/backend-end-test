<?php

/* UploadBundle:Default:layout.html.twig */
class __TwigTemplate_0c74841f858838126f7d80b0b193bd9fb23c908c4d62640d0b4e5acb22ec6a65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'jsFile' => array($this, 'block_jsFile'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css\">-->
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/styles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "        ";
        $this->displayBlock('jsFile', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <div id=\"container\">
            <div id=\"boxHeader\">";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["titleHeader"]) ? $context["titleHeader"] : null), "html", null, true);
        echo "</div>
            <div id=\"boxNavigation\">
                    <ul id=\"boxLi\">
                        <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getUrl("upload_profilListPage");
        echo "\"><i class=\"fa fa-table\"></i>LIST</a></li>
                        <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getUrl("upload_formPage");
        echo "\"><i class=\"fa fa-upload\"></i>UPLOAD</a></li>
                        <div class=\"clearBox\"></div>
                    </ul>
            </div>
            <div id=\"boxBody\">
                <div id=\"boxContain\">
                    <div id=\"boxFlashMessage\">
                        ";
        // line 26
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array(), "any", false, true), "flashbag", array(), "any", false, true), "get", array(0 => "errorList"), "method", true, true)) {
            // line 27
            echo "                            ";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "errorList"), "method")) > 0)) {
                // line 28
                echo "                                <ul>
                                    ";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "errorList"), "method"));
                foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
                    // line 30
                    echo "                                        <li>";
                    echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
                    echo "</li>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 32
                echo "                                </ul>   
                            ";
            }
            // line 34
            echo "                        ";
        }
        // line 35
        echo "                    </div>
                    <div id=\"contain\">
                        ";
        // line 37
        $this->displayBlock('body', $context, $blocks);
        // line 38
        echo "                    </div>
                </div>
            </div>
        </div>
            ";
        // line 42
        $this->displayBlock('javascripts', $context, $blocks);
        // line 43
        echo "    </body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["titlePage"]) ? $context["titlePage"] : null), "html", null, true);
    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 10
    public function block_jsFile($context, array $blocks = array())
    {
    }

    // line 37
    public function block_body($context, array $blocks = array())
    {
    }

    // line 42
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "UploadBundle:Default:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 42,  139 => 37,  134 => 10,  129 => 9,  123 => 5,  118 => 43,  116 => 42,  110 => 38,  108 => 37,  104 => 35,  101 => 34,  97 => 32,  88 => 30,  84 => 29,  81 => 28,  78 => 27,  76 => 26,  66 => 19,  62 => 18,  56 => 15,  48 => 11,  45 => 10,  43 => 9,  39 => 8,  35 => 7,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}{{ titlePage }}{% endblock %}</title>*/
/*         <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->*/
/*         <link href="{{ asset('bundles/framework/font-awesome-4.5.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />*/
/*         <link href="{{ asset('bundles/framework/css/styles.css') }}" rel="stylesheet" type="text/css" />*/
/*         {% block stylesheets %}{% endblock %}*/
/*         {% block jsFile %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         <div id="container">*/
/*             <div id="boxHeader">{{ titleHeader }}</div>*/
/*             <div id="boxNavigation">*/
/*                     <ul id="boxLi">*/
/*                         <li><a href="{{ url('upload_profilListPage') }}"><i class="fa fa-table"></i>LIST</a></li>*/
/*                         <li><a href="{{ url('upload_formPage') }}"><i class="fa fa-upload"></i>UPLOAD</a></li>*/
/*                         <div class="clearBox"></div>*/
/*                     </ul>*/
/*             </div>*/
/*             <div id="boxBody">*/
/*                 <div id="boxContain">*/
/*                     <div id="boxFlashMessage">*/
/*                         {% if app.session.flashbag.get('errorList') is defined %}*/
/*                             {% if app.session.flashbag.get('errorList')|length > 0 %}*/
/*                                 <ul>*/
/*                                     {% for flashMessage in app.session.flashbag.get('errorList') %}*/
/*                                         <li>{{ flashMessage }}</li>*/
/*                                     {% endfor %}*/
/*                                 </ul>   */
/*                             {% endif %}*/
/*                         {% endif %}*/
/*                     </div>*/
/*                     <div id="contain">*/
/*                         {% block body %}{% endblock %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*             {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
