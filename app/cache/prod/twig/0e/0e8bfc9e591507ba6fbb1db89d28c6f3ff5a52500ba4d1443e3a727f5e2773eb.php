<?php

/* UploadBundle:Form:upload.html.twig */
class __TwigTemplate_b6345c8aee010f4d8f33b71205d09ec2fb95ce2100b41ca267cb9354eddc2bd6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("UploadBundle:Default:layout.html.twig", "UploadBundle:Form:upload.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UploadBundle:Default:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "\t<div id=\"boxUpload\">
\t\t";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "
\t</div>
\t<div id=\"boxError\">
\t\t";
        // line 8
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 9
            echo "\t\t    <ul>
\t\t        ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["error"]) ? $context["error"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["errorMessage"]) {
                // line 11
                echo "\t\t            <li>";
                echo twig_escape_filter($this->env, $context["errorMessage"], "html", null, true);
                echo "</li>
\t\t        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['errorMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "\t\t    </ul>
\t\t";
        }
        // line 15
        echo "\t</div>
";
    }

    // line 18
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "UploadBundle:Form:upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 18,  63 => 15,  59 => 13,  50 => 11,  46 => 10,  43 => 9,  41 => 8,  35 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends 'UploadBundle:Default:layout.html.twig' %}*/
/* */
/* {% block body %}*/
/* 	<div id="boxUpload">*/
/* 		{{ form(form) }}*/
/* 	</div>*/
/* 	<div id="boxError">*/
/* 		{% if error %}*/
/* 		    <ul>*/
/* 		        {% for errorMessage in error %}*/
/* 		            <li>{{ errorMessage }}</li>*/
/* 		        {% endfor %}*/
/* 		    </ul>*/
/* 		{% endif %}*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}{% endblock %}*/
/* */
/* */
