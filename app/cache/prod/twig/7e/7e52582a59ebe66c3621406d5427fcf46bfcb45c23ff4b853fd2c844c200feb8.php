<?php

/* UploadBundle:Profil:displayList.html.twig */
class __TwigTemplate_23a9dcd7912a88c666c03ca2bbdd4a2416cb6864832f002db942e36ad9300afa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("UploadBundle:Default:layout.html.twig", "UploadBundle:Profil:displayList.html.twig", 1);
        $this->blocks = array(
            'jsFile' => array($this, 'block_jsFile'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UploadBundle:Default:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_jsFile($context, array $blocks = array())
    {
        // line 4
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/jquery-2.2.0.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/jquery.serializeToJSON.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/formValidation.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/formProfil.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/eventProfil.js"), "html", null, true);
        echo "\"></script>
\t
";
    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "<div id=\"boxProfil\">
\t<div id=\"boxBtnAdd\"><button id=\"btnAdd\" class=\"btn\"><i class=\"fa fa-plus\"></i>NEW PROFIL</button></div>
\t<input type=\"hidden\" id=\"baseUrl\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["baseUrl"]) ? $context["baseUrl"] : null), "html", null, true);
        echo "\"/>
\t<input type=\"hidden\" id=\"actionForm\" value=\"\"/>
\t<input type=\"hidden\" id=\"idProfil\" value=\"\"/>
\t<div id=\"profilContain\">
\t\t<div id=\"boxTable\">
\t\t\t";
        // line 20
        if (array_key_exists("profils", $context)) {
            // line 21
            echo "\t        \t";
            if ((twig_length_filter($this->env, (isset($context["profils"]) ? $context["profils"] : null)) > 0)) {
                // line 22
                echo "\t\t\t\t\t<table id=\"tableProfil\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<th>FIRSTNAME</th>
\t\t\t\t\t\t\t<th>LASTNAME</th>
\t\t\t\t\t\t\t<th>GENDER</th>
\t\t\t\t\t\t\t<th>MAIL</th>
\t\t\t\t\t\t\t<th>ACTIONS</th>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t";
                // line 31
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["profils"]) ? $context["profils"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["profil"]) {
                    // line 32
                    echo "\t                            <tr>
\t                            \t<td id=\"firstname_";
                    // line 33
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "firstname", array()), "html", null, true);
                    echo "</td>
\t                            \t<td id=\"lastname_";
                    // line 34
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "lastname", array()), "html", null, true);
                    echo "</td>
\t                            \t<td id=\"gender_";
                    // line 35
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "gender", array()), "html", null, true);
                    echo "</td>
\t                            \t<td id=\"mail_";
                    // line 36
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "mail", array()), "html", null, true);
                    echo "</td>
\t                            \t<td>
\t                            \t\t<button class=\"btn btnAction btnEdit\" id=\"btnEdit_";
                    // line 38
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\"><i class=\"fa fa-pencil\"></i>EDIT</button>
\t                            \t\t<button class=\"btn btnAction btnDelete\" id=\"btnDelete_";
                    // line 39
                    echo twig_escape_filter($this->env, $this->getAttribute($context["profil"], "id", array()), "html", null, true);
                    echo "\"><i class=\"fa fa-trash\"></i>DELETE</button>
\t                            \t</td>
\t                            </tr>
\t                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['profil'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 43
                echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t";
            } else {
                // line 46
                echo "\t\t\t\t\t<div id=\"emptyTxt\"><i class=\"fa fa-exclamation-triangle\"></i>EMPTY LIST</div>
\t\t\t\t";
            }
            // line 47
            echo "\t\t
\t      \t";
        }
        // line 49
        echo "\t\t</div>
\t\t<div id=\"boxForm\">
\t\t\t<div id=\"boxFormProfil\"></div>
\t\t</div>
\t\t<div class=\"clearBox\"></div>
\t</div>
\t<div class=\"clearBox\"></div>
</div>
";
    }

    // line 59
    public function block_javascripts($context, array $blocks = array())
    {
        // line 60
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/eventProfil.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t\t\$(document).ready(function(){

\t\t\teventProfil.init();

\t\t});\t
\t</script>
";
    }

    public function getTemplateName()
    {
        return "UploadBundle:Profil:displayList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 60,  159 => 59,  147 => 49,  143 => 47,  139 => 46,  134 => 43,  124 => 39,  120 => 38,  113 => 36,  107 => 35,  101 => 34,  95 => 33,  92 => 32,  88 => 31,  77 => 22,  74 => 21,  72 => 20,  64 => 15,  60 => 13,  57 => 12,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'UploadBundle:Default:layout.html.twig' %}*/
/* */
/* {% block jsFile %}*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/jquery-2.2.0.min.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/jquery.serializeToJSON.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/formValidation.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/formProfil.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/eventProfil.js') }}"></script>*/
/* 	*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/* <div id="boxProfil">*/
/* 	<div id="boxBtnAdd"><button id="btnAdd" class="btn"><i class="fa fa-plus"></i>NEW PROFIL</button></div>*/
/* 	<input type="hidden" id="baseUrl" value="{{ baseUrl }}"/>*/
/* 	<input type="hidden" id="actionForm" value=""/>*/
/* 	<input type="hidden" id="idProfil" value=""/>*/
/* 	<div id="profilContain">*/
/* 		<div id="boxTable">*/
/* 			{% if profils is defined %}*/
/* 	        	{% if profils|length > 0 %}*/
/* 					<table id="tableProfil">*/
/* 						<thead>*/
/* 							<th>FIRSTNAME</th>*/
/* 							<th>LASTNAME</th>*/
/* 							<th>GENDER</th>*/
/* 							<th>MAIL</th>*/
/* 							<th>ACTIONS</th>*/
/* 						</thead>*/
/* 						<tbody>*/
/* 							{% for profil in profils %}*/
/* 	                            <tr>*/
/* 	                            	<td id="firstname_{{profil.id}}">{{profil.firstname}}</td>*/
/* 	                            	<td id="lastname_{{profil.id}}">{{profil.lastname}}</td>*/
/* 	                            	<td id="gender_{{profil.id}}">{{profil.gender}}</td>*/
/* 	                            	<td id="mail_{{profil.id}}">{{profil.mail}}</td>*/
/* 	                            	<td>*/
/* 	                            		<button class="btn btnAction btnEdit" id="btnEdit_{{profil.id}}"><i class="fa fa-pencil"></i>EDIT</button>*/
/* 	                            		<button class="btn btnAction btnDelete" id="btnDelete_{{profil.id}}"><i class="fa fa-trash"></i>DELETE</button>*/
/* 	                            	</td>*/
/* 	                            </tr>*/
/* 	                        {% endfor %}*/
/* 						</tbody>*/
/* 					</table>*/
/* 				{% else %}*/
/* 					<div id="emptyTxt"><i class="fa fa-exclamation-triangle"></i>EMPTY LIST</div>*/
/* 				{% endif %}		*/
/* 	      	{% endif %}*/
/* 		</div>*/
/* 		<div id="boxForm">*/
/* 			<div id="boxFormProfil"></div>*/
/* 		</div>*/
/* 		<div class="clearBox"></div>*/
/* 	</div>*/
/* 	<div class="clearBox"></div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* 	<script type="text/javascript" src="{{ asset('bundles/framework/js/eventProfil.js') }}"></script>*/
/* 	<script type="text/javascript">*/
/* 		$(document).ready(function(){*/
/* */
/* 			eventProfil.init();*/
/* */
/* 		});	*/
/* 	</script>*/
/* {% endblock %}*/
