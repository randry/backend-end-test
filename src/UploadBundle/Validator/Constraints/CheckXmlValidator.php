<?php

namespace UploadBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class CheckXmlValidator extends ConstraintValidator
{

	 public function validate($value, Constraint $constraint)
    {

        if($value->getClientMimeType() != $constraint->validFormat)
        	$this->context->buildViolation($constraint->messageInvalidFile)->addViolation();

    }
	
}
