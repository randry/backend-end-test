<?php

namespace UploadBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 */
class CheckXml extends Constraint
{

	public $messageInvalidFile = "Invalid file";
	public $messageInvalidXml = "Invalid xml file";
	public $validFormat = "text/xml";

}
