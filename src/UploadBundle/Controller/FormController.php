<?php

namespace UploadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UploadBundle\Entity\Xml;
use UploadBundle\Form\XmlType;
use Symfony\Component\Validator\Constraints as Assert;


class FormController extends Controller
{

	public $fileName;
	public $pathFile;
	public $errorList = [];

	/**
	* upload file
	*
	* @Route("/upload")
	* @Template()
	*/
    public function uploadAction(Request $request)
    {

    	$upload = new Xml();
        $uploadForm = $this->createForm(XmlType::class, $upload);
        $uploadForm->handleRequest($request);

    	if($uploadForm->isSubmitted() && $request->isMethod('POST')){
    		//get file post
			$file = $uploadForm->get('xml_file')->getNormData();

			$errors = $this->get('validator')->validate($upload);

			if(count($errors) == 0){

    	    	$xmlServices = $this->get('uploadBundle.XmlServices');
    	    	// get file object
    	    	$setFile = $xmlServices->setFileObject((object)$file);
    	    	// generate file name
				$fileName = $xmlServices->generateFileName();
				if($fileName)
					// copy file in path directory
					$moveFile = $xmlServices->moveFile();
				if($moveFile){
					// validate data in file
					$dataXml = $xmlServices->isValidXml();
					if(is_object($dataXml)){
						if(count($dataXml) == 0){

							$this->errorList[] = "Profil not found";
							$removeFile = $xmlServices->removeFile();
							return array('titlePage' => 'CONSULTIS - Upload', 'titleHeader' => 'UPLOAD FORM', 'form' => $uploadForm->createView(), 'error' => $this->errorList);

						}
						else{

							$profilServices = $this->get('uploadbundle.ProfilServices');
							// count profil
							$sizeProfil = count($dataXml);
							// number of profil add successful 
							$sizeProfilAdd = 0;

							// loop profil
							for($n = 0; $n < $sizeProfil; $n++){

								if(!empty($dataXml->item[$n])){

									$addProfil = false;
									// valid profil data
									$validData = $profilServices->isValidData("upload", $dataXml->item[$n], (int)$n);

	                        		if(!is_object($validData))
	                        			$this->errorList[] = $validData;
	                        		else
	                        			// save profil data
	                        			$addProfil = $profilServices->addProfil($dataXml->item[$n]);

	                        		if($addProfil)
	                        			$sizeProfilAdd++;

                        		}

							}

							// remove xml file
							$removeFile = $xmlServices->removeFile();

							if(empty($sizeProfilAdd)){
								$this->errorList[] = "All profil are invalid";
								return array('titlePage' => 'CONSULTIS - Upload', 'titleHeader' => 'UPLOAD FORM', 'form' => $uploadForm->createView(), 'error' => $this->errorList);
							}
							else{

								if(count($this->errorList) > 0){
									foreach($this->errorList as $value){
										$this->get('session')->getFlashBag()->add('errorList', $value);
									}
									
								}

								return $this->redirect($this->generateUrl('upload_profilListPage'));

							}

						}

					}
					else{

						$this->errorList[] = "Invalid xml File";
						$removeFile = $xmlServices->removeFile();
						return array('titlePage' => 'CONSULTIS - Upload', 'titleHeader' => 'UPLOAD FORM', 'form' => $uploadForm->createView(), 'error' => $this->errorList);

					}

				}else{

					$this->errorList[] = "Permission write denied in path";
					return array('titlePage' => 'CONSULTIS - Upload', 'titleHeader' => 'UPLOAD FORM', 'form' => $uploadForm->createView(), 'error' => $this->errorList);

				}

			}else{

	    		return array('titlePage' => 'CONSULTIS - Upload', 'titleHeader' => 'UPLOAD FORM', 'form' => $uploadForm->createView(), 'error' => '');
			}			

		}
		else{

			return array('titlePage' => 'CONSULTIS - Upload', 'titleHeader' => 'UPLOAD FORM', 'form' => $uploadForm->createView(), 'error' => $this->errorList);

		}

    }

}
