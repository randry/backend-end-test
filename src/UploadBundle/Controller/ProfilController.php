<?php

namespace UploadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use UploadBundle\Repository\ProfilRepository;


class ProfilController extends Controller
{
    
    /**
	* display all profil
	*
	* @Route("/list")
	* @Template()
	*/
	public function displayListAction(){

		// get all profil
        $profilList = $this->getDoctrine()->getManager()->getRepository('UploadBundle:Profil')->findBy(array(), array('firstname'=>'asc'));

		return array(
			'titlePage' => 'CONSULTIS - Profil list',
			'titleHeader' => 'PROFIL LIST',
			'profils' => $profilList,
			'baseUrl' => $this->getParameter('ajaxBaseUrlDev')
			);

    }

    /**
    * get all profil
    *
    * @Route("/getAllProfil")
    * param object $request
    */
    public function getAllProfilAction(Request $request){

    	// check if is ajax request and post method
        if($request->isXMLHttpRequest() && $request->isMethod('POST')){

    		if($request->get('action') == "getAllProfil"){
                // get all profil
    			$profilList = $this->getDoctrine()->getManager()->getRepository('UploadBundle:Profil')->getAllProfil();
    			if(is_array($profilList))
    				return new JsonResponse(json_encode($profilList));
    			else
    				return new Response(0);
    		}
    	}
    }

    /**
    * get profil by Id
    *
    * @Route("/getProfil")
    * param object $request
    */
    public function getProfilAction(Request $request){

    	// check if is ajax request and post method
        if($request->isXMLHttpRequest() && $request->isMethod('POST')){

    		if($request->get('data')){
    			// get data post
    			$data = json_decode($request->get('data'), true);

				if(is_int($data["idProfil"])){

					$em = $this->getDoctrine()->getManager();
                    // get profil by Id
    				$result = $em->getRepository('UploadBundle:Profil')->getProfil($data["idProfil"]);
    				if($result){
						return new JsonResponse(json_encode($result));
					}else{
						return new Response(0);
					}
				}
				else{
					return new Response(0);	
				}

    		}

    	}

    }

    /**
    * add profil
    *
    * @Route("/add")
    * parama object $request
    */
    public function addAction(Request $request){

    	// check if is ajax request and post method
        if($request->isXMLHttpRequest() && $request->isMethod('POST')){
            // get data post
    		$data = json_decode($request->get('data'));

    		// validate data
    		$profilServices = $this->get('uploadbundle.ProfilServices');
    		// clean data
			$cleanData = $profilServices->cleanData($data);
			// validate data
			$validData = $profilServices->isValidData("add", $cleanData);
			if(is_array($validData)){
				return new JsonResponse(json_encode($validData));
			}
			else{

				// save profil
    			$profilServices = $this->get('uploadbundle.ProfilServices'); 
                $result = $profilServices->addProfil($data);
                if($result)
					return new Response(1);

			}	

    	}

    }

    /**
    * edit profil
    *
    * @Route("/edit")
    * param object $request
    * return int
    *
    */
    public function editAction(Request $request){

        // check if is ajax request and post method
    	if($request->isXMLHttpRequest() && $request->isMethod('POST')){
            // get data post
    		$data = json_decode($request->get('data'));
            // get id post
    		$id = json_decode($request->get('id'));
    		// validate data
    		$profilServices = $this->get('uploadbundle.ProfilServices');
    		// clean data
			$cleanData = $profilServices->cleanData($data);
			// validate data
			$validData = $profilServices->isValidData("edit", $cleanData);
			if(is_array($validData)){
				return new JsonResponse(json_encode($validData));
			}
			else{

				// update profil
    			$em = $this->getDoctrine()->getManager();
    			$result = $em->getRepository('UploadBundle:Profil')->editProfil($data, $id);
                if($result)
				    return new Response(1);

			}

    	}

    }

    /**
    * delete profil
    *
    * @Route("/delete")
    * param object $request
    * return int
    */
    public function deleteAction(Request $request){

        // check if is ajax request and post method
    	if($request->isXMLHttpRequest() && $request->isMethod('POST')){

    		if(($request->get('data'))){
    			// get data post
    			$data = json_decode($request->get('data'), true);

				if(is_int($data["idProfil"])){

					$em = $this->getDoctrine()->getManager();
                    // delete profil
    				$result = $em->getRepository('UploadBundle:Profil')->deleteProfil($data["idProfil"]);
    				if($result)
						return new Response(1);
					else
						return new Response(0);
				}
				else{
					return new Response(0);	
				}

    		}

    	}

    }

}
