<?php

namespace UploadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Profil entity
 *
 * @ORM\Table(name="profil")
 * @ORM\Entity(repositoryClass="UploadBundle\Repository\ProfilRepository")
 */
class Profil
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @var datetime
    *
    * @ORM\Column(name="date_processing", type="datetime", nullable=true)
    * @Assert\DateTime()
    */
    private $date_processing;

    /**
    * @ORM\Column(name="firstname", type="string", length=50, nullable=false)
    * @Assert\NotBlank(message="Please enter firstname !")
    * @Assert\Regex("/^[a-zA-Z| |]+$/i", message="Invalid firstname !")
    *   
    */
    private $firstname;

    /**
    * @ORM\Column(name="lastname", type="string", length=50, nullable=false)
    * @Assert\NotBlank(message="Please enter lastname !")
    * @Assert\Regex("/^[a-zA-Z| |]+$/i", message="Invalid lastname !")
    */
    private $lastname;

    /**
    *
    * @ORM\Column(name="gender", type="string", length=8, nullable=false)
    * @Assert\NotBlank(message="Please select gender !")
    * @Assert\Choice(choices = {"male", "female"}, message="Invalid gender !")
    *
    */
    private $gender;

    /**
    *
    * @ORM\Column(name="mail", type="string", length=50, nullable=false)
    * @Assert\NotBlank(message="Please enter mail !")
    * @Assert\Email(message="Invalid mail !")
    *
    */
    private $mail;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date_processing
     *
     * @param \DateTime $dateProcessing
     * @return Profil
     */
    public function setDateProcessing($dateProcessing)
    {
        $this->date_processing = $dateProcessing;

        return $this;
    }

    /**
     * Get date_processing
     *
     * @return \DateTime 
     */
    public function getDateProcessing()
    {
        return $this->date_processing;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Profil
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Profil
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Profil
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Profil
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }
}
