<?php

namespace UploadBundle\Entity;

use UploadBundle\Validators;
use Symfony\Component\Validator\Constraints as Assert;
use UploadBundle\Validator\Constraints as XmlAssert;


/**
 * Xml entity
 *
 */
class Xml
{

    /**
    *
    * @Assert\NotBlank(message="Please upload xml file.")
    * @XmlAssert\CheckXml
    */
    private $xml_file;

    /**
     * get $xml_file
     *
     * @return object
     */
    public function getXmlFile()
    {
        return $this->xml_file;
    }

    /**
     * Set $xml_file
     *
     * @param object $file
     * @return object
     */
    public function setXmlFile($file)
    {
        $this->xml_file = $file;

        return $this;
    }
}
