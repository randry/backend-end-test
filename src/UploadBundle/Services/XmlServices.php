<?php

namespace UploadBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Container;

/**
 * XmlServices
 * 
 */
class XmlServices
{

	private $fileObject;
	private $pathFile;
	public $nameFile;
	public $fileError = [];

	public function __construct(Container $container){

		$this->pathFile = $container->getParameter('xml_path');

	}

	/**
    * validate xml data
    *
    * return object
    *
    */
	public function isValidXml(){

		if(file_exists($this->pathFile . $this->nameFile)){

			$xmlData = simplexml_load_file($this->pathFile . $this->nameFile);
			if(!$xmlData)
        		return false;
        	else
        		return $xmlData;	

		}

	}
	
	/**
    * generate file name
    *
    * return boolean
    *
    */
	public function generateFileName(){

		$isFileExist = true;
		$nameFile = $this->fileObject->getClientOriginalName();

		do{

			if(!file_exists($this->pathFile . $nameFile))
				$isFileExist = false;
			else
				$nameFile = uniqid().".xml";

		}while($isFileExist === true);

		$this->nameFile = $nameFile;

		return true;

	}

	/**
    * copy file
    *
    * return boolean
    *
    */
	public function moveFile(){

		if(is_writable($this->pathFile)){
			$this->fileObject->move($this->pathFile, $this->nameFile);
			if(file_exists($this->pathFile . $this->nameFile))
				return true;
		}
		else{
			return false;
		}

	}

	/**
    * remove file
    *
    */
	public function removeFile(){

		if(file_exists($this->pathFile . $this->nameFile))
			unlink($this->pathFile . $this->nameFile);

	}

	/**
    * set $fileObject
    *
    * param object $file
    */
	public function setFileObject($file){

		if(is_object($file))
			$this->fileObject = $file;

	}

	/**
    * get $fileObject
    *
    * return object
    */
	public function getFileObject(){

		return $this->fileObject;

	}	

	/**
    * set $pathFile
    *
    * param string $path
    */
	public function setPath($path){

		$this->pathFile = $path;

	}

	/**
    * get $pathFile
    *
    * return string
    */
	public function getPath(){

		return $this->pathFile;

	}

}
