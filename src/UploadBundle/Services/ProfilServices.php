<?php

namespace UploadBundle\Services;

use Doctrine\ORM\EntityManager;
use UploadBundle\Entity\Profil;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

use Symfony\Component\DependencyInjection\Container;

/**
 * ProfilServices
 * 
 */
class ProfilServices
{

	public $data;
	public $dataError;
	private $entityManager;
	private $container;

	public function __construct(EntityManager $manager, Container $container)
    {

        $this->entityManager = $manager;
        $this->container = $container;

    }

    /**
    * clean data, remove start/end space and mutli space in string
    * param objct $data
    * return object
    *
    */
	public function cleanData($data){
		
		foreach($data as $key => $value){
			$value = trim($value);
			$value = preg_replace("/[[:blank:]]+/"," ", $value);
			$data->$key = $value;
		}

		return $data;

	}

	/**
    * validate data
    *
    * param string $action
    * param object $data
    * param int $key
    * return object
    *
    */
	public function isValidData($action, $data, $key = false)
	{

		$profil = new Profil();

    	$profil->setDateProcessing(new \DateTime());
    	$profil->setFirstname($data->{'firstname'});
    	$profil->setLastname($data->{'lastname'});
    	$profil->setGender($data->{'gender'});
    	$profil->setMail($data->{'mail'});

    	$errors = $this->container->get('validator')->validate($profil);

		if(count($errors) > 0){

			if($action == "upload"){

		       	$this->dataError = "Invalid profil in Item ".$key;
		    	return $this->dataError;

	    	}
	    	else{

	    		foreach ($errors as $error){

	    			$key = ucfirst(str_replace(array('[', ']'), '', $error->getPropertyPath()));

	    			$this->dataError[$key] = $error->getMessage();
	    		}

	    		return $this->dataError;
	    	}

		}
		else{
			return $profil;		
		}

	}

	/**
    * add profil
    *   
    * param object $data
    * return boolean
    *
    */
	public function addProfil($data)
    {

    	$profil = new Profil();

    	$profil->setFirstname($data->{'firstname'});
    	$profil->setLastname($data->{'lastname'});
    	$profil->setGender($data->{'gender'});
    	$profil->setMail($data->{'mail'});

        $em = $this->entityManager;
        $profil->setDateProcessing(new \DateTime());
        $em->persist($profil);
        $em->flush();

        return true;
		
    }

}
