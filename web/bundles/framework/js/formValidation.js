var errorList = {};

var noBlank = ['firstname', 'lastname', 'gender', 'mail'];

var myRegExp = {
	integer: /^[0-9]+$/i,
	string: /^[a-zA-Z| |-]+$/i,
	gender: /(male)|(female)/,
	alphanum: /[a-z0-9]/i,
	address: /^[a-zA-Z0-9 ,-]+$/i,
	mail: /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/i,
	title: /^[a-z|.|]+$/i,
	decimal: /^[0-9|.|]+$/i
};

var formRules = {
	firstname: myRegExp['string'],
	lastname: myRegExp['string'],
	gender: /^(male|female)$/,
	mail: myRegExp['mail']
};

var formValidation = {

	validateDataForm: function(data){

		var isValid;
		var value;

		for(var i in data){

			// check if field exist in form rules
			if(formRules.hasOwnProperty(i)){

				if(errorList.hasOwnProperty(i))
					this.removeField(i);
				// clean data
				value = data[i];
				value = value.trim().replace(/ {1,}/g," ");
				// check if blank
				if(noBlank.indexOf(i) > -1 && value.length == 0){

				 		var message;
				 		switch(i){
				 			case 'gender':
				 				message = "Please select "+i+" !";
				 			break;
				 			default:
				 				message = "Please enter "+i+" !";
				 			break;
				 		}

				 		errorList[i] = message;
				}

				// check if valid
				if(value.length > 0){
					isValid = formRules[i].test(value);
					if(!isValid)
						errorList[i] = "Invalid "+i+" !";
				}

			}

		}

		if(Object.keys(errorList).length)
			return false;
		else
			return true;

	},
	validateData: function(field, value){

		// check if field is exist in form rules
		if(formRules.hasOwnProperty(field)){
			// clean value
			value = value.trim().replace(/ {1,}/g," ");
			if(errorList.hasOwnProperty(field))
				this.removeField(field);
			// check if blank
			if(noBlank.indexOf(field) > -1 && value.length == 0)
			 	// check if field exist in error list
			 	errorList[field] = "Please enter "+field+" !";
			
			// check if valid
			if(value.length > 0){
				var isValid = formRules[field].test(value);
				if(!isValid)
				 	errorList[field] = "Invalid "+field+" !";
				
			}

		}

		if(errorList.hasOwnProperty(field))
			return false;
		else
			return true;

	},
	displayErrorMessage: function(errors){

		for(var key in errors){
			var upCaseKey = key.charAt(0).toUpperCase() + key.slice(1);
			if($('#check'+upCaseKey).length)
				$('#check'+upCaseKey).text(errors[key]);
		}
	},
	displayErrorField: function(field){

		var upCaseField = field.charAt(0).toUpperCase() + field.slice(1);
		if($('#check'+upCaseField).length)
			$('#check'+upCaseField).text(errorList[field]);
	},
	removeField: function(field){
		delete errorList[field];
	},
	resetInputCheck: function(){

		var inputCheck = document.querySelectorAll('.inputCheck');
		for(var n = 0; n < inputCheck.length; n++){
			if($(inputCheck[n]).length)
				$(inputCheck[n]).text('');
		}
	}

};