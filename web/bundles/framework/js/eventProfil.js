var eventProfil = {

	init: function(){

		this.addEvent();
		this.editEvent();
		this.deleteEvent();

	},
	addEvent: function(){

			var btnAdd = document.getElementById('btnAdd');
			btnAdd.addEventListener('click', function(){
				formProfil.displayFormAction("add");
				document.getElementById('actionForm').value = "add";
			});
	},
	editEvent: function(){

		var btnEdit = document.querySelectorAll('.btnEdit');
		for(var i=0; i < btnEdit.length; i++){
			btnEdit[i].addEventListener('click', function(){
				eventProfil.disabledAllBtn();
				var idAttr = this.id.split("_");
				var idProfil = idAttr[1];
				formProfil.displayFormAction("edit", idProfil);
		        document.getElementById('idProfil').value = idProfil;
				document.getElementById('actionForm').value = "edit";
			});
		}
	},
	disabledAllBtn: function(){
		var allBtn = document.querySelectorAll('.btn');
		for(var i=0; i < allBtn.length; i++)
			allBtn[i].disabled = true;
		
	},
	enabledAllBtn: function(){
		var allBtn = document.querySelectorAll('.btn');
		for(var i=0; i < allBtn.length; i++)
			allBtn[i].disabled = false;
		
	},
	deleteEvent: function(){

		var btnDelete = document.querySelectorAll('.btnDelete');
		for(var i=0; i < btnDelete.length; i++){
			btnDelete[i].addEventListener('click', function(e){
				e.preventDefault();
				var confirm = false;
				confirm = window.confirm("Delete this Profil ?");
				if(confirm){
					document.getElementById('actionForm').value = "delete";
					formProfil.removeForm();
					var idAttr = this.id.split("_");
					var idProfil = idAttr[1];
					formProfil.deleteAction(idProfil);
					
				}
			});
		}
	}

};
