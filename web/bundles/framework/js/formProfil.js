var formProfil = {

	inputForm : document.querySelectorAll('.inputForm'),
	displayFormAction: function(action, id){

		var titleForm;
		var titleBtn;

		document.getElementById('boxFormProfil').innerHTML = "";	
		$('#boxFormProfil').load('/form/formProfil.html', function(){

			switch (action){
			    case "add":
			    	titleForm = "NEW PROFIL";
			    	titleBtn = "ADD";
			        break;
			    case "edit":
			        titleForm = "EDIT PROFIL";
			        titleBtn = "EDIT";
			        formProfil.getProfil(id);
			        break;
			}

			document.getElementById('titleForm').textContent = titleForm;
			document.getElementById('btnSubmit').textContent = titleBtn;

			formProfil.cancelEvent();
			formProfil.validateEvent();
			formProfil.submitEvent(action);		

		});
		
		
	},
	cleanData: function(data){

		for(var i in data){

			var value = data[i];
			value = value.trim().replace(/ {1,}/g," ");
			data[i] = value;			

		}

		return data;

	},
	getDataForm: function(){

		var inputData = $('#formProfil').serializeToJSON();
		var radioData = {};

		$("#formProfil input[type='radio']").each(function(element){

			if(!radioData.hasOwnProperty(this.name) || radioData[this.name] == ""){
				if(this.checked)	
					radioData[this.name] = this.value;
				else
					radioData[this.name] = "";
			}

		});

		var data = $.extend(inputData, radioData);

		return data;

	},
	getProfil: function(id){

		var url = document.getElementById('baseUrl').value+"getProfil";
		var data = {idProfil: parseInt(id)};
		$.ajax({
			url: url,
			data: "data="+JSON.stringify(data),
			method: 'POST',
			dataType: 'JSON',
			success: function(response){

				var data = JSON.parse(response);
				
				var result = {};
				// generate object variable
				for(var i in data[0]){

					result[i] = data[0][i];
				}

				formProfil.fillForm(result);

			},
			complete: function(response){

				eventProfil.enabledAllBtn();
			}

		});

	},
	fillForm: function(data){

		for(var key in data){

			var value = data[key];
			var upperCasekey = key.charAt(0).toUpperCase() + key.slice(1);
			if(isNaN(value)){
				value += '';
				var upperCaseValue = value.charAt(0).toUpperCase() + value.slice(1);
			}
			if($('#formProfil #input'+upperCasekey).length || $('#formProfil input[name="'+key+'"]').length){

				// fill input text
				if($('#formProfil #input'+upperCasekey).length)
					$('#formProfil #input'+upperCasekey).val(value);
				// checked radio
				if(key == "gender")
					$('#formProfil #input'+upperCasekey + upperCaseValue).prop("checked", true);

			}
		}

	},
	deleteAction: function(id){

		var url = document.getElementById('baseUrl').value+"delete";
		var data = {idProfil: parseInt(id)};
		data = "data="+JSON.stringify(data);
		this.ajaxRequest(url, data);

	},
	refreshTableAction: function(data){

		var url = document.getElementById('baseUrl').value+"getAllProfil";
		var data = "action=getAllProfil";
		$.ajax({
			url: url,
			data: data,
			method: 'POST',
			dataType: 'JSON',
			success: function(response){

				if(response != 0){
					var data = JSON.parse(response);
					if(data.length > 0)
						// refresh table
						formProfil.generateTable(data);
					else
						formProfil.displayEmptyAlert();

				}
			}

		});

	},
	displayEmptyAlert: function(){

		var html = '';
		html = '<div id="emptyTxt"><i class="fa fa-exclamation-triangle"></i>EMPTY LIST</div>';

		$('#boxTable').html(html);

	},
	generateTable: function(data){

		var html = "";
		html += '<table id="tableProfil">';
			html += '<thead>';
				html += '<th>FIRSTNAME</th>';
				html += '<th>LASTNAME</th>';
				html += '<th>GENDER</th>';
				html += '<th>MAIL</th>';
				html += '<th>ACTIONS</th>';
			html += '</thead>';
			html += '<tbody>';
				for(var i in data){
					html += '<tr>';
						html += '<td id="firstname_'+data[i]['id']+'">'+data[i]['firstname']+'</td>';
						html += '<td id="lastname_'+data[i]['id']+'">'+data[i]['lastname']+'</td>';
						html += '<td id="gender_'+data[i]['id']+'">'+data[i]['gender']+'</td>';
						html += '<td id="mail_'+data[i]['id']+'">'+data[i]['mail']+'</td>';
						html += '<td>';
						html += '<button class="btn btnAction btnEdit" id="btnEdit_'+data[i]['id']+'"><i class="fa fa-pencil"></i>EDIT</button>';
						html += '<button class="btn btnAction btnDelete" id="btnDelete_'+data[i]['id']+'"><i class="fa fa-trash"></i>DELETE</button>';
						html += '</td>';
					html += '</tr>';
				}
			html += '</tbody>';
		html += '<table/>';

		$('#boxTable').html(html).promise().done(function(elem){
			eventProfil.editEvent();
			eventProfil.deleteEvent();
		});

	},
	ajaxRequest: function(url, data){

		$.ajax({
			url: url,
			data: data,
			method: 'POST',
			dataType: 'JSON',
			success: function(response){

				if(!isNaN(response) && response == 1){

					var action = document.getElementById("actionForm").value;
					switch(action){
					    case "add":
					    	formProfil.removeForm();
					    	alert("Profil created successfully !");
					        break;
					    case "edit":
					    	formProfil.removeForm();
					    	alert("Profil updated successfully !");
					    break;
					    case "delete":
					    	alert("Profil removed successfully !");
					    break;
					}

				}
				else{

					var errors = JSON.parse(response);
					formValidation.displayErrorMessage(errors);		

				}

			},
			complete : function(){
				// enabled all button
				eventProfil.enabledAllBtn();
				// refresh table profil list
				formProfil.refreshTableAction(data);
			}
		});

	},
	removeForm: function(){
		if($('#formProfil').length){
			document.getElementById("boxFormProfil").innerHTML = "";
		}
		
	},
	cancelEvent: function(){

		document.querySelector('#btnCancel').addEventListener('click', function(){
				formProfil.removeForm();
			});

	},
	validateEvent: function(){

		if($('.inputForm').length){

			$('.inputForm').each(function(){
				// blur event
				this.addEventListener('blur', function(e){	
					e.preventDefault();
					var isValid = formValidation.validateData(this.name, this.value);
					if(!isValid)
						formValidation.displayErrorField(this.name);

				});

				// keyup event
				this.addEventListener('keyup', function(e){
					e.preventDefault();
					var name = this.name; 
					var upCaseName = name.charAt(0).toUpperCase() + name.slice(1);

					if($('#check'+upCaseName).length){
						$('#check'+upCaseName).text("");
					}

				});
			
			});
		}
		
	},
	submitEvent: function(action){
		
		if($('#btnSubmit').length){

			document.getElementById('btnSubmit').addEventListener('click', function(e){
				e.preventDefault();
				var confirm = false;
				// get entry form
				var data = formProfil.getDataForm();
				// clean entry form
				data = formProfil.cleanData(data);
				// validate entry form
				var isValid = formValidation.validateDataForm(data);
				if(isValid){
					// get current action
					var action = document.getElementById('actionForm').value;
					confirm = window.confirm(action.charAt(0).toUpperCase() + action.slice(1)+" this Profil ?");
				if(confirm){
						eventProfil.disabledAllBtn();
						var url;
						var baseUrl = document.getElementById('baseUrl').value;
						var dataPost;
						switch(action){
						    case "add":
						    	//formProfil.addAction();
						    	url = baseUrl+"add";
						    	dataPost = "data="+JSON.stringify(data);
						        break;
						    case "edit":
						    	url = baseUrl+"edit";
						    	var id = {idProfil: parseInt(document.getElementById('idProfil').value)};
						    	dataPost = "data="+JSON.stringify(data)+"&id="+JSON.stringify(id);
						        break;
						}

						formProfil.ajaxRequest(url, dataPost);
					}
				}
				else{
					formValidation.resetInputCheck();
					formValidation.displayErrorMessage(errorList);
				}

			});
		}	
	}

};